WITH_SDDM  ?= no

PREFIX     ?= /usr
SYSCONFDIR ?= /etc
BINDIR     ?= $(PREFIX)/bin
DATADIR    ?= $(PREFIX)/share
LIBEXECDIR ?= $(PREFIX)/libexec

.PHONY: build clean install check requires
.DEFAULT: build

GENERATED := $(patsubst %.in,%,$(wildcard sway/*.in sway/*/*.in ))
VARIABLES := PREFIX SYSCONFDIR DATADIR LIBEXECDIR

sway/%: sway/%.in
	sed "$(foreach var,$(VARIABLES),s^@$(var)@^$($(var))^i;)" $^ > $@
	sed '$(foreach var,$(VARIABLES),s^$$$(var)\b^$($(var))^i;)' $^ > $@

build: $(GENERATED)

clean:
	rm -f $(GENERATED)
	rm -rf test/

install-sddm:
	install -D -m 0755 -pv -t $(DESTDIR)$(LIBEXECDIR)      sddm/sddm-compositor-sway
	install -D -m 0644 -pv -t $(DESTDIR)$(SYSCONFDIR)/sway sddm/sddm-greeter.config
	install -D -m 0644 -pv -t $(DESTDIR)$(PREFIX)/lib/sddm/sddm.conf.d sddm/wayland-sway.conf

install-sway: build
	install -D -m 0644 -pv -t $(DESTDIR)$(SYSCONFDIR)/sway          sway/config
	install -D -m 0644 -pv -t $(DESTDIR)$(SYSCONFDIR)/sway          sway/environment
	install -D -m 0755 -pv -t $(DESTDIR)$(BINDIR)                   sway/start-sway
	install -D -m 0644 -pv -t $(DESTDIR)$(DATADIR)/wayland-sessions sway/sway.desktop
	install -D -m 0644 -pv -t $(DESTDIR)$(DATADIR)/sway/config.d    sway/config.d/*.conf
	install -D -m 0644 -pv -t $(DESTDIR)$(DATADIR)/sway/config.live.d sway/config.live.d/*.conf
	install -D -m 0755 -pv -t $(DESTDIR)$(LIBEXECDIR)/sway          scripts/sway/*

install-swaylock:
	install -D -m 0644 -pv -t $(DESTDIR)$(SYSCONFDIR)/swaylock swaylock/config

.PHONY: install-sddm install-sway install-swaylock

ifeq ($(WITH_SDDM),yes)
install: install-sddm
endif

install: install-sway install-swaylock

check:
	mkdir -p test
	export WLR_BACKENDS=headless WLR_RENDERER=pixman XDG_RUNTIME_DIR="$(CURDIR)/test" SRCDIR="$(CURDIR)"; \
		/usr/bin/sway --debug --validate --config $(CURDIR)/sway/config.test

requires: | build
	@./scripts/requires.py sway/config
